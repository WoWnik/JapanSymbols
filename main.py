import io
import random

width = 50
height = 10

alphabet = [chr(i) for i in range(int("30A0", 16), int("30FF", 16) + 1)]
alphabet = [
    "ア", "イ", "ウ", "エ", "オ", "ン",
    "カ", "キ", "ク", "ケ", "コ",
    "ガ", "ギ", "グ", "ゲ", "ゴ",
    "サ", "シ", "ス", "セ", "ソ",
    "ザ", "ジ", "ズ", "ゼ", "ゾ",
    "タ", "チ", "ツ", "テ", "ト",
    "ダ", "ヂ", "ヅ", "デ", "ド",
    "ハ", "ヒ", "フ", "ヘ", "ホ",
    "バ", "ビ", "ブ", "ベ", "ボ",
    "パ", "ピ", "プ", "ペ", "ポ",
    "ナ", "ニ", "ヌ", "ネ", "ノ",
    "マ", "ミ", "ム", "メ", "モ",
    "ラ", "リ", "ル", "レ", "ロ",
    "ヤ", "ユ", "ヨ",
    "ワ", "ヲ"
]
print(alphabet)

f = io.open("out.txt", "w", encoding='utf8')
for h in range(height):
    for w in range(width):
        letter = random.choice(alphabet)
        f.write(letter)
    f.write("\n")

f.close()
